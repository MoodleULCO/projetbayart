package com.example.projetbayart;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Classe permettant de construire un objet Quiz (contenant une catégorie et une liste de Questions)
 */
public class Quiz implements Serializable {
    private String category;
    private ArrayList<Question> questions;

    public Quiz(String category, ArrayList<Question> questions) {
        this.category = category;
        this.questions = questions;
    }

    public String getCategory() {
        return category;
    }
    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void addQuestions(ArrayList<Question> questions) {
        for(Question q: questions) {
            this.questions.add(q);
        }
    }
}
