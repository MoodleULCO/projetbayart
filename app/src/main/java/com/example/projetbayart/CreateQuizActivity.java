package com.example.projetbayart;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class CreateQuizActivity extends AppCompatActivity {
    public static final String QUIZ = "com.example.projetbayart.QUIZ";

    private ArrayList<Question> questions;
    private ArrayList<String> questionsLabels;
    private ArrayAdapter<String> adapter;
    private EditText category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_quiz);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // initialisation des variables
        this.category = (EditText) findViewById(R.id.categoryInput);
        this.category.setText("");

        this.questions = new ArrayList<>();
        this.questionsLabels = new ArrayList<>();
        // On crée l'adaptater à l'aide la l'ArrayList "items"
        ListView lv = findViewById(R.id.list);
        this.adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, questionsLabels);
        lv.setAdapter(adapter);

        FloatingActionButton fab = findViewById(R.id.fab);
        // En cas de click sur le bouton "+", appelle la méthode 'addQuestion'
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addQuestion();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // En cas de résultat provenant de l'ajout d'une question
        if(requestCode == 1 && resultCode == RESULT_OK) {
            // récupération de la question créé
            Question q = (Question) data.getSerializableExtra(AddQuestionActivity.QUESTION);
            // Ajout a la liste des questions et on affiche son label dans la listview
            questions.add(q);
            questionsLabels.add(q.getLabel());
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * Méthode appelée lors du clic sur "Confirmer"
     * @param v : view
     */
    public void validate(View v) {
        Intent intent = new Intent();
        // On transmet à l'activité le Quiz construit après vérification des champs
        if(!"".equals(this.category.getText().toString()) && !questions.isEmpty()) {
            Quiz quiz = new Quiz(this.category.getText().toString(), questions);
            intent.putExtra(QUIZ, quiz);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            // En cas d'erreur un message est affiché
            Toast.makeText(this, "Données manquantes pour soumettre le questionnaire", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Méthode appelée lors du clic sur "Annuler"
     * @param v : view
     */
    public void cancel(View v) {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    /**
     * Méthode permettant de créer une nouvelle question, elle appelle l'activité correspondante
     */
    public void addQuestion() {
        Intent intent = new Intent(this, AddQuestionActivity.class);
        startActivityForResult(intent, 1);
    }
}