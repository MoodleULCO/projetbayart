package com.example.projetbayart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.HashMap;

public class ScoresActivity extends AppCompatActivity {
    private HashMap<String, String> scores;
    private static DecimalFormat formatter = new DecimalFormat("#.##");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);

        Intent intent = getIntent();
        this.scores = (HashMap<String, String>) intent.getSerializableExtra(MainActivity.SCORES);
        updateScores();
        calcMean();
    }

    /**
     * Méthode permettant de mettre à jour l'affichage des scores
     */
    private void updateScores() {
        if(scores.size() > 0) {
            String scoreView = "";

            // Pour chaque questionnaire réalisé, on affiche le score
            for (String category : scores.keySet()) {
                scoreView += "Note questionnaire " + category + " : " + scores.get(category) + "\n";
            }

            TextView scoresView = findViewById(R.id.scoresView);
            scoresView.setText(scoreView);
        }
    }

    /**
     * Méthode permettant de calculer la moyenne des scores
     */
    private void calcMean() {
        if(scores.size() > 0) {
            double mean = 0.0;

            // Pour chaque questionnaire ayant été réalisé, on rapporte le score sur 20 et on l'ajoute au total
            for (String category : scores.keySet()) {
                String[] str = scores.get(category).split("/");
                mean += (Double.parseDouble(str[0]) / Double.parseDouble(str[1]))*20;
            }

            // On fait la moyenne de ces scores et on affiche

            mean = mean/ scores.size();

            TextView meanView = findViewById(R.id.meanView);
            meanView.setText("Note moyenne : " + formatter.format(mean) +"/20");
        }
    }
    /**
     * Méthode appelée lors du clic sur "Annuler"
     * @param v : view
     */
    public void validate(View v) {
        //Retour a la page précédente
        finish();
    }
}