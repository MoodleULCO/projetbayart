package com.example.projetbayart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class PlayQuizActivity extends AppCompatActivity {
    public static final String SCORE = "com.example.projetbayart.SCORE";

    private ListView answers;
        private ArrayList<Question> questions;
        private ArrayList<String> answersList = new ArrayList<>();
        private Question currentQuestion;
        private ArrayAdapter<String> arrayAdapter;

    private int questionNumbers = 0;
    private int currentQuestionNumber = 1;
    private int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_quiz);

        // Initialisation des variables et récupération du quiz à jouer
        Quiz quiz = (Quiz) getIntent().getSerializableExtra(MainActivity.QUIZ);

        TextView catLabel = (TextView) findViewById(R.id.category);
        catLabel.setText(quiz.getCategory());
        questions = quiz.getQuestions();
        questionNumbers = questions.size();

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_single_choice, answersList);

        answers = (ListView) findViewById(R.id.answers);
        answers.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        answers.setAdapter(arrayAdapter);

        // Chargement de la 1ère question
        loadQuestion();
    }

    /**
     * Méthode permettant de charger aléatoirement une question
     */
    private void loadQuestion() {
        // On restaure l'état de selecteurs
        answers.requestLayout();
        answers.clearChoices();

        // Génération d'un numéro random de question
        int random = new Random().nextInt(questions.size());

        // On stocke la question
        currentQuestion = questions.get(random);
        TextView questionNumber = (TextView) findViewById(R.id.questionNumber);

        // On affiche le numéro de la question
        String questionNum = "Question " + currentQuestionNumber + "/" + questionNumbers;
        questionNumber.setText(questionNum);

        // On affiche la question
        TextView questionLabel = (TextView) findViewById(R.id.CategoryLabel);
        questionLabel.setText(currentQuestion.getLabel());

        answersList.clear();
        // On affiche la liste des réponses correspondant à cette question
        for(String answer : currentQuestion.getAnswers()) {
            answersList.add(answer);
        }

        arrayAdapter.notifyDataSetChanged();
    }

    /**
     * Méthode appelée lors du clic sur "Valider"
     * @param v : view
     */
    public void validate(View v) {
        // On récupére l'item selectionné dans la listView
        SparseBooleanArray checkedItems = answers.getCheckedItemPositions();

        // On vérifie si la question choisie est bonne, auquel cas on incrémente le score
        if(checkedItems != null) {
            String selectedAnswer = "";
            for (int i = answers.getCount() - 1; i >= 0; i--) {
                if (checkedItems.get(i)) {
                    selectedAnswer = answersList.get(i);
                }
            }

            if (selectedAnswer.equals(currentQuestion.getGoodAnswer())) {
                score++;
            }
            questions.remove(currentQuestion);
        }

        // Dans le cas où il n'y a plus de question à poser, on renvoie le score en résultat
        if(questions.size() == 0) {
            Intent intent = new Intent();
            intent.putExtra(SCORE, Integer.toString(score));
            setResult(RESULT_OK, intent);
            finish();
        } else {
            //S'il existe encore des question, on charge la suivante
            currentQuestionNumber++;
            loadQuestion();
        }
    }

    /**
     * Méthode appelée lors du clic sur "Stopper"
     * @param v
     */
    public void stop(View v) {
        Intent intent = new Intent();
        // Renvoi un score égal à 0 comme résultat
        intent.putExtra(SCORE, "0");
        setResult(RESULT_CANCELED, intent);
        finish();
    }
}