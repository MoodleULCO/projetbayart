package com.example.projetbayart;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

public class FileHelper {
    // Path menant aux fichiers recherchés
    private String path;

    public FileHelper(String path) {
        this.path = path;
    }

    /**
     * Méthode permettant de recupérer un objet Quiz à partir d'un nom de fichier
     * @param fileName : nom de fichier
     * @return Quiz construit avec les données contenues dans le fichier
     */
    public Quiz getQuizFromFile(String fileName) {
        ArrayList<Question> questionsList = new ArrayList<>();
        ArrayList<String> answers = new ArrayList<>();
        Quiz quiz = null;

        BufferedReader bufferedreader = null;
        File directory = new File(path);
        File file = new File(directory, fileName);

        try {
            bufferedreader = new BufferedReader(new FileReader(file));

            String strCurrentLine;
            while((strCurrentLine = bufferedreader.readLine()) != null) {
                String[] questions = strCurrentLine.split(";");
                for(String str : questions) {
                    String[] question = str.split("&");

                    answers = new ArrayList<>();
                    answers.add(question[1]);
                    answers.add(question[2]);
                    answers.add(question[3]);

                    questionsList.add(new Question(question[0], answers,question[4]));
                }
                String category = fileName.replace(".txt", "");
                quiz = new Quiz(category, questionsList);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedreader != null)
                    bufferedreader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return quiz;
    }

    /**
     * Méthode permettant de stocker dans un fichier les données contenu dans un objet de type Quiz
     * @param quiz : objet à stocker
     */
    public void storeQuiz(Quiz quiz){
        BufferedWriter bufferedWriter = null;
        PrintWriter printWriter = null;

        try {
            File directory = new File(path);
            File file = new File(directory, quiz.getCategory() + ".txt");

            if(!file.exists()) {
                file.createNewFile();
            }

            bufferedWriter = new BufferedWriter(new FileWriter(file));
            printWriter = new PrintWriter(bufferedWriter);

            for(Question question : quiz.getQuestions()) {
                String questionStr = question.getLabel() + "&";

                for(String answer : question.getAnswers()) {
                    questionStr += answer + "&";
                }

                questionStr += question.getGoodAnswer();
                questionStr += ";";

                printWriter.print(questionStr);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(printWriter != null)
                    printWriter.close();
                if(bufferedWriter != null)
                    bufferedWriter.close();
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Méthode permettant de récupérer une HashMap contenant l'ensemble des scores sauvegardés sous la forme Catégorie -> Score
     * @return scores sauvegardés
     */
    public HashMap<String, String> getScores() {
        HashMap<String, String> scores = new HashMap<>();

        BufferedReader bufferedreader = null;
        File directory = new File(path);
        File file = new File(directory, "scores.txt");

        try {
            bufferedreader = new BufferedReader(new FileReader(file));

            String strCurrentLine;
            while((strCurrentLine = bufferedreader.readLine()) != null) {
                String[] score = strCurrentLine.split(";");
                for(String items : score) {
                    String[] item = items.split("&");
                    scores.put(item[0],item[1]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedreader != null)
                    bufferedreader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return scores;
    }

    /**
     * Méthode permettant de sauvegarder les scores contenus dans une HashMap dans un fichier "scores.txt"
     * @param scores : scores à sauvegarder
     */
    public void storeScores(HashMap<String, String> scores) {
        BufferedWriter bufferedWriter = null;
        PrintWriter printWriter = null;

        try {
            File directory = new File(path);
            File file = new File(directory, "scores.txt");

            if(file.exists()) {
                file.delete();
            }
            file.createNewFile();

            bufferedWriter = new BufferedWriter(new FileWriter(file));
            printWriter = new PrintWriter(bufferedWriter);

            for(String category : scores.keySet()) {
                String questionStr = category + "&" + scores.get(category) +";";
                printWriter.print(questionStr);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(printWriter != null)
                    printWriter.close();
                if(bufferedWriter != null)
                    bufferedWriter.close();
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
