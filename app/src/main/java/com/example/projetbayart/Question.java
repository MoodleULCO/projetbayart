package com.example.projetbayart;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Classe permettant de construire une Question (contient un label, une liste de réponses et une bonne réponse)
 */
public class Question implements Serializable {
    private String label;
    private ArrayList<String> answers;
    private String goodAnswer;

    public Question(String label, ArrayList<String> answers, String goodAnswer) {
        this.label = label;
        this.answers = answers;
        this.goodAnswer = goodAnswer;
    }

    public String getLabel() {
        return label;
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public String getGoodAnswer() {
        return goodAnswer;
    }
}
