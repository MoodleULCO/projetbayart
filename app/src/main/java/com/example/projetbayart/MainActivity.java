package com.example.projetbayart;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    // Constantes appelées pour passer des paramètres aux Intent
    public static final String QUIZ = "com.example.projetbayart.QUIZ";
    public static final String SCORES = "com.example.projetbayart.SCORES";
    public static final String PASSWORD = "MDP";

    // Valeurs stockées pour jouer les Quiz
    private ArrayList<String> categories = new ArrayList<>();
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<Quiz> quizs = new ArrayList<>();
    private HashMap<String, String> scores = new HashMap<>();
    private Quiz currentQuiz;

    // Variable permettant de charger ou créer des sauvegardes dans des fichiers
    private FileHelper fileHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fileHelper = new FileHelper(getApplicationContext().getFilesDir().toString());

        final Spinner catSpinner = (Spinner) findViewById(R.id.categorySpinner);
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        catSpinner.setAdapter(arrayAdapter);

        loadScore();
        load();
        update();

        Button confirm = (Button) findViewById(R.id.startQuiz);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                start(catSpinner.getSelectedItem().toString());
            }
        });
    }

    /**
     * Méthode permettant de démarrer un questionnaire
     * @param category
     */
    public void start(String category) {
        Intent intent = new Intent(this, PlayQuizActivity.class);
        // Récupération de la catégorie selecionnée et lancement du Quiz
        currentQuiz = quizs.get(categories.indexOf(category));
        intent.putExtra(QUIZ, currentQuiz);
        startActivityForResult(intent, 1);
    }

    /**
     * Méthode permettant de récupérer un Quiz à partir de son fichier et de le stocker dans une liste
     * @param fileName
     */
    public void loadQuiz(String fileName) {
        quizs.add(fileHelper.getQuizFromFile(fileName));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // En cas de résultat de l'activité PlayQuizActivity
        if(requestCode == 1 && (resultCode == RESULT_OK || resultCode == RESULT_CANCELED)) {
            // On récupère le score obtenu au Quiz, on le stocke et on retire le Quiz de la liste des Quiz auxquels l'utilisateur peut encore répondre
            String score = data.getStringExtra(PlayQuizActivity.SCORE) + "/" + currentQuiz.getQuestions().size();
            scores.put(currentQuiz.getCategory(), score);

            this.categories.remove(currentQuiz.getCategory());
            this.quizs.remove(currentQuiz);

            Spinner catSpinner = (Spinner) findViewById(R.id.categorySpinner);
            catSpinner.setSelection(0);

            updateScores();
        }
        // En cas de résultat de l'activité CreateQuizActivity, on récupère le Quiz créé et on le sauvegarde ou le met à jour
        if(requestCode == 2 && resultCode == RESULT_OK) {
            Quiz quiz = (Quiz) data.getSerializableExtra(CreateQuizActivity.QUIZ);
            createQuiz(quiz);
            reload();

            if(scores.containsKey(quiz.getCategory())) {
                scores.remove(quiz.getCategory());
                updateScores();
            }
        }
        arrayAdapter.notifyDataSetChanged();
    }

    /**
     * Méthode permettant de charger les quiz et les scores
     */
    private void load() {
        String path = getApplicationContext().getFilesDir().toString();
        File directory = new File(path);
        File[] files = directory.listFiles();
        for(File file : files) {
            // Pour chaque fichiers contenus dans le répertoire correspondant à getApplicationContext().getFilesDir()
            System.out.println("FILE STORED : " + file.getName());
            // On récupère le nom de ce fichier sans ".txt", ce qui correspondra à la catégorie et on vérifie qu'il ne s'agit pas de "scores.txt"
            String category = file.getName().replace(".txt", "");
            if(!file.getName().equals("scores.txt") && !isScoreForCategory(category)) {
                categories.add(category);
                // On charge le Quiz correspondant
                loadQuiz(file.getName());
            }
        }
        arrayAdapter.notifyDataSetChanged();
    }

    /**
     * Méthode permettant de créer un quiz
     * @param quiz
     */
    private void createQuiz(Quiz quiz) {
        Quiz q = quiz;
        // On vérifie si le quiz existe ou non
        if(categories.contains(quiz.getCategory())) {
            // S'il existe, on ajoute les question au quiz existant, sinon on créé un nouveau quiz
            q = quizs.get(categories.indexOf(quiz.getCategory()));
            q.addQuestions(quiz.getQuestions());
        }

        // On stocke le quiz dans un fichier
        fileHelper.storeQuiz(q);
    }

    /**
     * Méthode permettant de mettre à jour la liste des quiz réalisables, en fonction des scores
     */
    private void update() {
        for(String category : categories) {
            // Si le quiz a été réalisé (qu'il existe un score pour ce quiz), on le retire de la liste
            if(isScoreForCategory(category)) {
                categories.remove(currentQuiz.getCategory());
                quizs.remove(currentQuiz);
                arrayAdapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * Méthode permettant de stocker les scores sauvegardés
     */
    private void updateScores() {
        fileHelper.storeScores(this.scores);
    }

    /**
     * Méthode permettant de vérifier s'il existe un score pour une catégorie donnée
     * @param category : catégorie a vérifier
     * @return true, si il en existe un, false sinon
     */
    private boolean isScoreForCategory(String category) {
        for(String categoryStored : scores.keySet()) {
            if (categoryStored.equals(category)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Méthide permettant d'accédér aux scores
     * @param v : view
     */
    public void showScores(View v) {
        Intent intent = new Intent(this,ScoresActivity.class);
        intent.putExtra(SCORES, scores);
        startActivity(intent);
    }

    /**
     *  Méthode permettant d'ouvrir une fenêtre de dialogue pour ajouter un item a la liste "items"
     * @param view :
     */
    public void openDialog(View view) {
        // Création de la fenêtre de dialogue
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Saisissez un Mot de passe Administrateur");

        // Création du champs de saisie d'un quiz'
        final EditText input = new EditText(this);
        input.setTransformationMethod(new PasswordTransformationMethod());
        builder.setView(input);

        // Création du bouton de confirmation
        builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // En cas de clic, demande un mot de passe
                // Vérification du mot de passe et appel de la méthode openCreateQuiz
                if(input.getText().toString().equals(PASSWORD)) {
                    openCreateQuiz();
                } else {
                    showErrorPassword();
                }
            }
        });

        // Création du bouton d'annulation
        builder.setNegativeButton("Anuuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // En cas de clic, fermeture de la boite de dialogue
                dialog.cancel();
            }
        });

        builder.show();
    }

    /**
     * Méthode affichant un message en cas de saisie éronnée du mot de passe
     */
    private void showErrorPassword() {
        Toast.makeText(this, "Mot de passe éronné", Toast.LENGTH_LONG).show();
    }

    /**
     * Méthode permettant d'accéder à la création d'un quiz
     */
    private void openCreateQuiz() {
        Intent intent = new Intent(this, CreateQuizActivity.class);
        startActivityForResult(intent, 2);
    }

    /**
     * Méthode permettant de stocker les scores sauvegardés dans une HashMap
     */
    private void loadScore() {
        this.scores = fileHelper.getScores();
    }

    /**
     * M2thode appelée pour réinitialiser les scores et les quiz
     * @param v
     */
    public void reinit(View v) {
        Spinner catSpinner = (Spinner) findViewById(R.id.categorySpinner);
        catSpinner.setSelection(0);
        this.scores.clear();
        this.quizs.clear();
        this.categories.clear();

        load();
        update();
        updateScores();
    }

    /**
     * Méthode permettant de vider les liste et de faire appel a load à nouveau
     */
    public void reload() {
        Spinner catSpinner = (Spinner) findViewById(R.id.categorySpinner);
        catSpinner.setSelection(0);
        this.quizs.clear();
        this.categories.clear();

        load();
    }
}