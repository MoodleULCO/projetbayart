package com.example.projetbayart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.opengl.EGLDisplay;
import android.opengl.ETC1;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.xml.sax.EntityResolver;

import java.util.ArrayList;

public class AddQuestionActivity extends AppCompatActivity {
    // Constante pour transmettre des données via putExtra
    public static final String QUESTION = "com.example.projetbayart.QUESTION";

    // Stockage des champs de saisie de texte
    private EditText label;
    private EditText firstAnswer;
    private EditText secondAnswer;
    private EditText thirdAnswer;

    //ArrayList d'indexs pour selectionner la bonne réponse à chaque question
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<String> indexes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_question);

        // Initialisation des champs
        this.label = (EditText) findViewById(R.id.questionLabel);
        this.label.setText("");

        this.firstAnswer = (EditText) findViewById(R.id.firstAnswer);
        this.firstAnswer.setText("");

        this.secondAnswer = (EditText) findViewById(R.id.secondAnswer);
        this.secondAnswer.setText("");

        this.thirdAnswer = (EditText) findViewById(R.id.thirdAnswer);
        this.thirdAnswer.setText("");

        // Ajout des valeurs [1,2,3] à la liste d'indexs
        indexes = new ArrayList<>();
        indexes.add("1");
        indexes.add("2");
        indexes.add("3");

        // On applique au spinner les valeurs présentes dans la liste d'index
        final Spinner indexSpinner = (Spinner) findViewById(R.id.goodAnswerIndex);
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, indexes);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        indexSpinner.setAdapter(arrayAdapter);
    }

    /**
     * Méthode appelée lors du clic sur "Confirmer"
     * @param v : view
     */
    public void validate(View v) {
        Intent intent = new Intent();
        String questionLabel = this.label.getText().toString();

        // Vérification  des champs de saisie
        ArrayList<String> answers = new ArrayList<>();
        if(!"".equals(this.firstAnswer.getText().toString()))
            answers.add(this.firstAnswer.getText().toString());
        if(!"".equals(this.secondAnswer.getText().toString()))
            answers.add(this.secondAnswer.getText().toString());
        if(!"".equals(this.thirdAnswer.getText().toString()))
            answers.add(this.thirdAnswer.getText().toString());

        // Récupération de l'index selectionné dans le spinner
        Spinner indexSpinner = (Spinner) findViewById(R.id.goodAnswerIndex);

        // On vérifie que tous les champs sont renseignés
        if(answers.size() == 3 && !"".equals(questionLabel)) {
            // Récupération de la réponse stockée à l'index selectionné
            String goodAnswer = answers.get(Integer.parseInt(indexSpinner.getSelectedItem().toString()) - 1);

            // Création de la question et envoie de celle-ci en réponse
            Question question = new Question(questionLabel, answers, goodAnswer);
            intent.putExtra(QUESTION, question);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            // Dans le cas où les champs sont mal renseignés, on affiche un message d'alerte
            Toast.makeText(this, "Données manquantes, veuillez vérifier", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Méthode appelée lors du clic sur "Annuler"
     * @param v : view
     */
    public void cancel(View v) {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }
}